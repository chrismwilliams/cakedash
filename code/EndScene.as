﻿package  {
	import flash.display.MovieClip;
	import flash.display.Stage;
	
	public class EndScene extends MovieClip {
		
		var EntityRef: Array = new Array();
		
		var NextScene: Object = null;
		
		var Unloading: Boolean = false;
		
		var mScore: Number;
		
		var endScreen1: MovieClip;
		
		var btn1: MovieClip;
		
		var btn2: MovieClip;

		public function EndScene(pScore: Number) {
			// constructor code
			mScore = pScore;
		}

		public function Initialise(mStage: Stage) {
			
			var CenterX: Number = mStage.stageWidth / 2;
			var CenterY: Number = mStage.stageHeight / 2;
			
			//Add End Screen
			endScreen1 = new end_screen(mScore);
			endScreen1.x = CenterX;
			endScreen1.y = CenterY;
			EntityRef.push(endScreen1);
			
			//Add Main Menu button
			btn1 = new button("MAIN MENU", CenterX - 170, CenterY - 20, this);
			EntityRef.push(btn1);
			
			//Add Restart button
			btn2 = new button("RESTART", CenterX + 170, CenterY - 20, this);
			EntityRef.push(btn2);
			
		}
		
		public function LoadContent(pStage: Stage) {
			
			for (var i = 0; i < EntityRef.length; i++) {

				pStage.addChild(EntityRef[i]);
				
			}
		}
		
		public function UnloadContent(caller: Object) {
			
			Unloading = true;
			NextScene.EntityRef = EntityRef;
			
			for (var j = 0; j < EntityRef.length; j++) {

				caller.stage.removeChild(EntityRef[j]);
			}

			EntityRef.length = 0;

			caller.CurrentScene = NextScene;
			caller.NewScene();
			
			
		}
		
		public function Update() {
			
		}
	}
	
}
