﻿package {

	import flash.display.Stage;
	import flash.display.MovieClip;
	import flash.events.KeyboardEvent;
	import flash.events.TimerEvent;

	public class GameScene extends MovieClip {

		var EntityRef: Array = new Array();

		var NextScene: Object = null;

		var Unloading: Boolean = false;

		var map: MovieClip;

		var grass: MovieClip;

		var infopanel1: MovieClip;

		var player1: MovieClip;

		var dropPin1: MovieClip;

		var _Arrow: MovieClip;

		var trafArray: Array = new Array();

		const NUM_OF_TCARS: Number = 6;


		public function GameScene() {
			// constructor code
		}


		public function Initialise(mStage: Stage) {

			var CenterX: Number = mStage.stageWidth / 2;

			//Add map
			map = new MapBackground();
			map.x = 0;
			map.y = 0;
			EntityRef.push(map);

			//Add Grass
			grass = new Grass();
			grass.x = 680;
			grass.y = 360;
			EntityRef.push(grass);

			//Add info panel
			infopanel1 = new infopanel(this);
			EntityRef.push(infopanel1);

			//Add player
			player1 = new playerSprite(trafArray, mStage, infopanel1, grass);
			player1.x = 297;
			player1.y = mStage.stageHeight / 1.2;
			player1.radius = player1.width / 2;
			player1.rotation -= 90;
			EntityRef.push(player1);

			//Add other cars
			for (var c = 0; c < NUM_OF_TCARS; c++) {
				trafArray[c] = new car(mStage, c);
				trafArray[c].radius = trafArray[0].width / 2;
				EntityRef.push(trafArray[c]);
			}


			//Add Drop pin
			dropPin1 = new DropPin(player1, infopanel1);
			EntityRef.push(dropPin1);

			//Add arrow
			_Arrow = new Arrow(player1, dropPin1);
			EntityRef.push(_Arrow);

			infopanel1.countdownTimer.start();

			mStage.addEventListener(KeyboardEvent.KEY_DOWN, KeyPressed);

			mStage.addEventListener(KeyboardEvent.KEY_UP, KeyReleased);

		}

		public function LoadContent(pStage: Stage) {

			for (var i = 0; i < EntityRef.length; i++) {

				pStage.addChild(EntityRef[i]);
			}

		}


		public function UnloadContent(caller: Object) {

			caller.stage.removeEventListener(KeyboardEvent.KEY_DOWN, KeyPressed);
			caller.stage.removeEventListener(KeyboardEvent.KEY_UP, KeyReleased);

			for (var t = 0; t < trafArray.length; t++) {
				
				if (trafArray[t].delayTimer != null) {
					trafArray[t].delayTimer.removeEventListener(TimerEvent.TIMER, trafArray[t].setAlive);
					trafArray[t].delayTimer = null;
				}
			}
			
			Unloading = true;
			NextScene.EntityRef = EntityRef;

			for (var j = 0; j < EntityRef.length; j++) {

				caller.stage.removeChild(EntityRef[j]);
			}

			EntityRef.length = 0;

			caller.CurrentScene = NextScene;
			caller.NewScene();


		}

		public function Update() {

			//Player update
			player1.Update();

			//info update
			infopanel1.Update();


			// traffic update
			for (var t = 0; t < trafArray.length; t++) {

				trafArray[t].Update();

			}

			//Pin update
			dropPin1.Update();

			//Arrow update
			_Arrow.Update();

		}

		public function EndGame() {

			NextScene = new EndScene(infopanel1.score);
		}


		public function KeyPressed(e: KeyboardEvent) {

			switch (e.keyCode) {

				case 65:

					player1.mInputComponent.left = true;
					break;

				case 68:

					player1.mInputComponent.right = true;
					break;

				case 87:

					player1.mInputComponent.up = true;
					break;

				case 83:

					player1.mInputComponent.down = true;
					break;

				case 16:

					player1.mInputComponent.shift = true;
					break;

				case 32:

					player1.mInputComponent.space = true;
					break;

			}


		}

		public function KeyReleased(e: KeyboardEvent) {

			switch (e.keyCode) {

				case 65:

					player1.mInputComponent.left = false;
					break;

				case 68:

					player1.mInputComponent.right = false;
					break;

				case 87:

					player1.mInputComponent.up = false;
					break;

				case 83:

					player1.mInputComponent.down = false;
					break;

				case 16:

					player1.mInputComponent.shift = false;
					break;

				case 32:

					player1.mInputComponent.space = false;
					break;

			}

		}

	}

}