﻿package {

	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.media.Sound;

	public class DropPin extends MovieClip {

		
		var mPlayer: MovieClip;
		var mPanel: MovieClip;

		var rPinDrop: int = 0;
		var oldPin: Number = 0;
		var pin: Boolean = false;
		var sound: Sound  = new pickUp();

		public function DropPin(pSprite: MovieClip, iPanel: MovieClip) {

			// constructor code
			
			mPlayer = pSprite;
			mPanel = iPanel

			PinPos();

		}

		public function PinPos() {

			// calls a random number which is then used in a switch case for the pins position.
			while (oldPin == rPinDrop) {
				rPinDrop = Math.ceil(Math.random() * 10);
			}
			
			//trace(rPinDrop);
			switch (rPinDrop) {
				case 1:
					this.x = 1150;
					this.y = 165;
					break;

				case 2:
					this.x = 60;
					this.y = 100;
					break;

				case 3:
					this.x = 300;
					this.y = 350;
					break;

				case 4:
					this.x = 600;
					this.y = 50;
					break;

				case 5:
					this.x = 700;
					this.y = 400;
					break;

				case 6:
					this.x = 700;
					this.y = 600;
					break;

				case 7:
					this.x = 1200;
					this.y = 600;
					break;

				case 8:
					this.x = 1030;
					this.y = 300;
					break;

				case 9:
					this.x = 60;
					this.y = 600;
					break;

				case 10:
					this.x = 370;
					this.y = 635;
					break;
			}
			pin = true;
			oldPin = rPinDrop;
			
		}

		public function Update() {


			if (pin) {
				if (this.hitTestObject(mPlayer)) {
					sound.play();
					mPanel.updatescore(1);
					mPanel.updatetime(5);
					mPanel.movetxt(this.x, this.y);
					pin = false;
					
				}
			}
			if (!pin) {
				PinPos();
				pin = true;
			}

		}

	}

}