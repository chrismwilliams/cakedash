﻿package {

	import flash.display.Stage;
	import flash.events.MouseEvent;
	import flash.display.MovieClip;

	public class PhysicsComponent {

		var possessed: MovieClip;

		const MAX_SPEED: Number = 5;

		var friction: Number = 0.95;
		var speed: Number = 0;
		var brakes: Number = 0.6;
		var acceleration: Number = 0.3;
		var radAngle: Number;
		var turnSpeed: Number = 1.2;
		var mapfriction: Number = 0.7;
		

		var rad: Number = Math.PI / 180

		public function PhysicsComponent(car: MovieClip) {

			// constructor code
			possessed = car;

		}

		public function Update() {

			physicsStuff();
			
		}

		public function physicsStuff() {

			//trace(speed);

			if (Math.abs(speed) < 0.2) {
				speed = 0;
			}
			
			possessed.velocity.x = Math.cos(possessed.rotation * rad) * speed;
			possessed.velocity.y = Math.sin(possessed.rotation * rad) * speed;
			possessed.x += possessed.velocity.x;
			possessed.y += possessed.velocity.y;

		}

	}

}