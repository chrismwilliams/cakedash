﻿package {
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.utils.getDefinitionByName;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.AntiAliasType;
	import flash.text.TextFormat;
	import flash.text.TextFormatAlign;
	import flash.text.TextFieldType;
	import flash.geom.ColorTransform;


	public class StartMenuScene extends MovieClip {

		var EntityRef: Array = new Array();

		var NextScene: Object = null;

		var Unloading: Boolean = false;

		var cakeArray: Array = new Array();

		const NUM_OF_CAKES: Number = 15;

		var btn: MovieClip;



		public function StartMenuScene() {
			// constructor code
		}



		public function Initialise(mStage: Stage) {

			var CenterX: Number = mStage.stageWidth / 2;

			//Add Welcome Text
			var txtHeader = new logo();
			txtHeader.x = CenterX;
			txtHeader.y = 150;

			EntityRef.push(txtHeader);


			//Cake array
			for (var c = 0; c < NUM_OF_CAKES; c++) {

				cakeArray[c] = new cake(mStage);
				cakeArray[c].x = Math.random() * mStage.stageWidth;
				cakeArray[c].y = 0 - cakeArray[c].height;

				cakeArray[c].velocity.x = Math.random() * 5;
				cakeArray[c].velocity.x *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;
				cakeArray[c].velocity.y = Math.random() * 10;
				cakeArray[c].radius = cakeArray[c].height / 2;
				cakeArray[c].scaleX = Math.random() * 2;
				cakeArray[c].scaleY = cakeArray[c].scaleX;
				EntityRef.push(cakeArray[c]);

			}

			//Add the button
			btn = new button("START GAME", CenterX, 600, this);
			EntityRef.push(btn);




		}


		public function LoadContent(pStage: Stage) {

			for (var i = 0; i < EntityRef.length; i++) {

				pStage.addChild(EntityRef[i]);

			}
		}

		public function UnloadContent(caller: Object) {

			Unloading = true;
			NextScene.EntityRef = EntityRef;

			for (var j = 0; j < EntityRef.length; j++) {

				caller.stage.removeChild(EntityRef[j]);
			}

			EntityRef.length = 0;

			caller.CurrentScene = NextScene;
			caller.NewScene();

		}

		public function Update() {

			for (var b = 0; b < cakeArray.length; b++) {

				cakeArray[b].Update();

			}

		}

	}

}