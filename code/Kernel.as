﻿package {

	import flash.display.MovieClip;

	import flash.events.Event;

	import flash.display.Stage;

	import flash.utils.Timer;

	import flash.events.TimerEvent;
	
	import flash.media.Sound;
	
	import flash.media.SoundChannel;

	public class Kernel extends MovieClip {


		var EntityList: Array = new Array();

		var CurrentScene: Object = null;

		
		public function Kernel() {
			// constructor code
			
			addEventListener(Event.ADDED_TO_STAGE, StartGame);
						

		} // End of Kernel



		public function StartGame(e: Event) {

			removeEventListener(Event.ADDED_TO_STAGE, StartGame);

			var cakeDashSound: Sound = new Music();
			cakeDashSound.play(0, 10);
						
			var period: Number = 1000 / 40;
			var gameTimer: Timer = new Timer(period);

			gameTimer.addEventListener(TimerEvent.TIMER, Update);
			gameTimer.start();
			
			CurrentScene = new StartMenuScene();
			NewScene();


		}

		public function Update(e: Event) {

			CurrentScene.Update();

			if (CurrentScene.NextScene != null && CurrentScene.Unloading == false) {

				UnloadContent();

			}

		}

		public function NewScene() {

			Initialise();
			LoadContent();
		}

		public function Initialise() {

			CurrentScene.EntityRef = EntityList;

			CurrentScene.Initialise(stage);

		}

		public function LoadContent() {

			CurrentScene.LoadContent(stage);

		}

		public function UnloadContent() {

			CurrentScene.UnloadContent(this);

		}


	}

}