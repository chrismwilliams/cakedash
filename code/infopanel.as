﻿package {

	import flash.display.MovieClip;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.text.TextFormat;
	import flash.media.Sound;


	public class infopanel extends MovieClip {

		var minutes: Number = int(1 * 60);
		var seconds: Number = 0;
		var total: Number = minutes + seconds;

		var numseconds: Number;
		var numminutes: Number;
		var displaysec: String;
		var displaymin: String;

		var updateDelay: Number = -1;
		var updateTimeAdded: Number;

		var carDelay: Number = -1;

		var countdownTimer: Timer

		var score: Number = 0;

		var gParent: Object;

		var sound: Sound = new hitsound();

		var speedChange: Boolean = false;

		public function infopanel(Parent: Object) {

			// constructor code
			this.x = 880;
			this.y = 80;

			gParent = Parent;

			this.score_txt.text = "Score : " + score;


			var _period: Number = 1000;

			countdownTimer = new Timer(_period);

			countdownTimer.addEventListener(TimerEvent.TIMER, countdown);



		}


		private function countdown(e: TimerEvent) {


			if (total <= 0) {
				this.time_txt.text = "00 : 00";
				this.removeEventListener(TimerEvent.TIMER, countdown);
				countdownTimer = null;

				gParent.EndGame();

			} else {

				if (updateDelay > -1) {

					updateDelay--;
				}



				total--;

			}
		}

		public function updatescore(num: Number) {

			score += num;

			this.score_txt.text = "Score : " + score.toString();
		}


		public function updatetime(timechange: Number) {

			total += timechange;
			updatetimetxt(timechange);
		}

		private function updatetimetxt(amount: Number) {

			updateTimeAdded = amount;
			updateDelay = 3;
		}

		public function movetxt(X: Number, Y: Number) {

			this.update_time_txt.x = X - 908;
			this.update_time_txt.y = Y - 105;


		}

		public function hitcar(carNum: Number) {

			sound.play();
			updatetime(-5);
			movetxt(gParent.trafArray[carNum].x, gParent.trafArray[carNum].y);

		}

		public function Update() {


			numseconds = total % 60;
			numminutes = Math.floor(total / 60);
			displaymin = numminutes.toString();
			displaysec = (numseconds < 10) ? "0" + numseconds.toString() : numseconds.toString();

			this.time_txt.text = "0" + displaymin + " : " + displaysec;


			if (updateDelay > -1) {

				this.update_time_txt.text = (updateTimeAdded > 0) ? "+" + updateTimeAdded.toString() : updateTimeAdded.toString();

			} else {

				this.update_time_txt.text = "";
			}

			if (!speedChange && total <= 15) {

				for (var c = 0; c < gParent.trafArray.length; c++) {

					gParent.trafArray[c].cspeed = 6;

				}
				speedChange = true;
			}

		}

	}

}