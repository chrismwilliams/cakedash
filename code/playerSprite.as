﻿package  {
	
	import flash.display.MovieClip;
	import flash.geom.Vector3D;
	import flash.display.Stage;
	
	public class playerSprite extends MovieClip {
		
		
		var mInputComponent: InputComponent = null;
		var mPhysicsComponent: PhysicsComponent = null;
		var mCollidableComponent: CollidableComponent = null;
		
		
		var velocity: Vector3D = new Vector3D();
		var radius: Number;
		
		var trafficArray: Array;
		
		var pStage: Stage;

		var iPanel: Object;
		
		var mGrass
		
		public function playerSprite(tArray: Array, mStage: Stage, mPanel: Object, pGrass: MovieClip) {
			
			// constructor code
			
			pStage = mStage;
			
			// car array
			trafficArray = tArray;
			
			//info panel
			iPanel = mPanel;
			
			//grass
			mGrass = pGrass;
			
			mPhysicsComponent = new PhysicsComponent(this);
			mInputComponent = new InputComponent(this);
			mCollidableComponent = new CollidableComponent(this);
			
			
		}
			
		public function Update() {
			
			mPhysicsComponent.Update();
			mInputComponent.Update()
			mCollidableComponent.Update();
							
		}
	}
	
}
