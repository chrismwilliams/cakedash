﻿package {
	import flash.display.MovieClip;
	import flash.events.MouseEvent;

	public class button extends MovieClip {

		var Clicked: Boolean;

		var SceneLink: Object;

		var ParentMenu: Object;


		public function button(Text: String, X: int, Y: int, pMenu: Object) {
			// constructor code

			this.bText.text = Text;

			this.x = X;

			this.y = Y;

			ParentMenu = pMenu;

			addEventListener(MouseEvent.CLICK, ClickEvent);

			switch (Text) {

				case "START GAME":
				case "RESTART":
					SceneLink = new GameScene();
					break;

				case "MAIN MENU":
					SceneLink = new StartMenuScene();
					break;

			}

		}


		public function ClickEvent(e: MouseEvent) {

			this.removeEventListener(MouseEvent.CLICK, ClickEvent);
			ParentMenu.NextScene = SceneLink;
		}
	}

}