﻿package {

	import flash.geom.Vector3D;
	import flash.display.MovieClip;

	public class InputComponent {

		var left: Boolean = false;
		var right: Boolean = false;
		var up: Boolean = false;
		var down: Boolean = false;
		var space: Boolean = false;
		var shift : Boolean = false;
		var player: MovieClip;


		public function InputComponent(car: MovieClip) {
			
			// constructor code
			player = car;
		}



		public function Update() {

				if (right) {

					if (Math.abs(player.mPhysicsComponent.speed) > player.mPhysicsComponent.acceleration) {
						player.rotation += player.mPhysicsComponent.turnSpeed * player.mPhysicsComponent.speed;
					}
				}

				if (left) {

					if (Math.abs(player.mPhysicsComponent.speed) > player.mPhysicsComponent.acceleration) {
						player.rotation -= player.mPhysicsComponent.turnSpeed * player.mPhysicsComponent.speed;
					}
				}

				if (up) {

					if (player.mPhysicsComponent.speed < player.mPhysicsComponent.MAX_SPEED) {

						player.mPhysicsComponent.speed += player.mPhysicsComponent.acceleration;

					} else {

						player.mPhysicsComponent.speed = player.mPhysicsComponent.MAX_SPEED;
					}

				} else {

					player.mPhysicsComponent.speed *= player.mPhysicsComponent.friction;
				}

				if (shift) {
					
					player.mPhysicsComponent.speed = player.mPhysicsComponent.speed * 5;
				}

				if (space) {
					
					player.mPhysicsComponent.speed *= player.mPhysicsComponent.brakes;

				}

				if (down) {
					
					if (player.mPhysicsComponent.speed > 0) {
						
						player.mPhysicsComponent.speed *= player.mPhysicsComponent.brakes;
					}

					if (player.mPhysicsComponent.speed <= 0) {

						if (player.mPhysicsComponent.speed > player.mPhysicsComponent.MAX_SPEED * -1) {

							player.mPhysicsComponent.speed -= player.mPhysicsComponent.acceleration * 0.7;

						}


					}

				}
			
		}
	}


}