﻿package {

	import flash.display.MovieClip;
	import flash.geom.Vector3D;
	import flash.display.Stage;


	public class cake extends MovieClip {

		var velocity: Vector3D = new Vector3D;
		var mStage: Stage;
		var radius: Number;


		public function cake(pStage: Stage) {
			
			// constructor code
			mStage = pStage;
		}

		public function Update() {

			if (this.y > mStage.stageHeight + this.height || this.x < 0 - this.width || this.x > mStage.stageWidth + this.width) {

				this.y = 0 - this.height;
				this.x = Math.random() * mStage.stageWidth;
				this.velocity.x = Math.floor(Math.random() * 5) + 1;
				this.velocity.x *= Math.floor(Math.random() * 2) == 1 ? 1 : -1;  
				this.velocity.y = Math.random() * 10;
				
			}
			
			this.y += this.velocity.y;
			this.x += this.velocity.x;
			this.rotation += this.velocity.x;

		}
	}

}