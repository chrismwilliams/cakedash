﻿package  {
	
	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.events.Event;
	
	public class Arrow extends MovieClip {

		var mPlayer: MovieClip;
		var mPin: MovieClip;
		
		var angle: Number = 0;
		var speed: Number = 3;
		var radius: Number = 75;
		
		
		public function Arrow(pPlayer: MovieClip, dPin: MovieClip) {

			// constructor code
			
			mPlayer = pPlayer;
			mPin = dPin;
			
					
		}
		
		public function Update() {
				
			var distance = Math.sqrt((mPlayer.x - mPin.x) * (mPlayer.x - mPin.x) + (mPlayer.y - mPin.y) * (mPlayer.y - mPin.y));
			if( distance > 150) {
				var rad:Number = angle * (Math.PI / 180); // Converting Degrees To Radians
				this.x = mPlayer.x + radius * Math.cos(rad); // Position The Arrow Along x-axis
				this.y = mPlayer.y + radius * Math.sin(rad); // Position The Arrow Along y-axis
				//angle += speed; // Object will orbit clockwise
				//angle -= speed; // Object will orbit counter-clockwise
				angle = (Math.atan2(mPin.y- this.y, mPin.x - this.x) * 180 / Math.PI);// Rotates the arrow in the direction of the drop pins
				this.rotation = (Math.atan2(mPin.y - this.y, mPin.x - this.x) * 180 / Math.PI); // Points the arrow in the direction of the drop pins
			}
		}

	}
	
}
