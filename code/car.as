﻿package {

	import flash.display.MovieClip;
	import flash.display.Stage;
	import flash.utils.Timer;
	import flash.events.TimerEvent;


	public class car extends MovieClip {

		var mStage: Stage;
		var radius: Number;

		var carnum: int;

		var xmspeed: int = 1;
		var ymspeed: int = 1;

		var xfacingdirection: int = 1;
		var yfacingdirection: int = 1;

		var cspeed: Number = 4;

		var alive: Boolean = true;
		
		var delayTimer: Timer = null;
		var setAlive: Function;
		
		
		public function car(pStage: Stage, number: int) {

			// constructor code
			pStage = mStage;

			carnum = number;

			startpos();


		}

		public function startpos() {

			switch (carnum) {

				case 0:
					this.x = 1150;
					this.y = 217;
					break;

				case 1:
					this.x = 275;
					this.y = 420;
					break;

				case 2:
					this.x = 535;
					this.y = 200;
					break;

				case 3:
					this.x = 580;
					this.y = 660;
					break;

				case 4:
					this.x = 1225;
					this.y = 460;
					this.carnum = 3;
					break;

				case 5:
					this.x = 300;
					this.y = 370;
					this.carnum = 2;
					break;
			}

		}


		public function car0() {

			this.x = this.x + (xmspeed * xfacingdirection);
			this.y = this.y + (ymspeed * yfacingdirection);



			if (this.y >= 217 && this.x <= 1257) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = 0;

			}

			if (this.x >= 1257 && this.y >= 25) {
				this.ymspeed = cspeed * -1;
				this.xmspeed = 0;
				this.rotation = -90;
			}

			if (this.y <= 25 && this.x >= 1250) {
				this.ymspeed = 0;
				this.xmspeed = cspeed * -1;
				this.rotation = -180;
			}

			if (this.x <= 1065 && this.y <= 25) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = -270;
			}

		}


		public function car1() {
			this.x = this.x + (xmspeed * xfacingdirection);
			this.y = this.y + (ymspeed * yfacingdirection);


			if (this.y >= 420 && this.x >= 260) {
				this.ymspeed = 0;
				this.xmspeed = cspeed * -1;
				this.rotation = -180;

			}
			if (this.x >= 260 && this.y <= 60) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = -270;
			}
			if (this.y <= 60 && this.x <= 260) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = -360;
			}
			if (this.x <= 60 && this.y >= 60) {
				this.ymspeed = cspeed * -1;
				this.xmspeed = 0;
				this.rotation = -90;
			}

		}


		public function car2() {
			this.x = this.x + (xmspeed * xfacingdirection);
			this.y = this.y + (ymspeed * yfacingdirection);

			if (this.x >= 300 && this.y >= 60) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = 0;
			}
			if (this.y >= 60 && this.x >= 535) {
				this.ymspeed = cspeed * -1;
				this.xmspeed = 0;
				this.rotation = -90;

			}
			if (this.x <= 655 && this.y >= 60 && this.y <= 65) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = 0;
			}
			if (this.y >= 60 && this.x >= 655) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = 90;
			}
			if (this.x >= 655 && this.y >= 220) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = 0;
			}
			if (this.y >= 220 && this.x <= 985 && this.x >= 980) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = 90;
			}
			if (this.x <= 985 && this.y >= 420) {
				this.ymspeed = 0;
				this.xmspeed = cspeed * -1;
				this.rotation = 180;
			}
			if (this.y >= 220 && this.x >= 300 && this.x <= 305) {
				this.ymspeed = cspeed * -1;
				this.xmspeed = 0;
				this.rotation = -90;
			}

		}

		public function car3() {
			this.x = this.x + (xmspeed * xfacingdirection);
			this.y = this.y + (ymspeed * yfacingdirection);

			if (this.x >= 297 && this.y >= 660) {
				this.ymspeed = 0;
				this.xmspeed = -cspeed;
				this.rotation = -180;
			}
			if (this.y >= 660 && this.x <= 297) {
				this.ymspeed = -cspeed;
				this.xmspeed = 0;
				this.rotation = -90;
			}
			if (this.x <= 297 && this.y >= 455 && this.y < 460) {
				this.ymspeed = 0;
				this.xmspeed = cspeed;
				this.rotation = 0;
			}
			if (this.y >= 455 && this.x >= 1225) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = 90;
			}
			if (this.x <= 1230 && this.y >= 570 && this.y < 575 && this.x >= 500) {
				this.ymspeed = 0;
				this.xmspeed = -cspeed;
				this.rotation = 180;
			}
			if (this.y >= 570 && this.x <= 580 && this.y <= 665 && this.x > 572) {
				this.ymspeed = cspeed;
				this.xmspeed = 0;
				this.rotation = -270;
			}


		}

		public function delayHit() {

			alive = false;
			gotoAndPlay(2);
			
			delayTimer = new Timer(3000);
			
			setAlive = function (e: TimerEvent) {

				delayTimer.removeEventListener(TimerEvent.TIMER, setAlive);
				gotoAndPlay(1);
				delayTimer = null;
				
				alive = true;
				
			}

			delayTimer.addEventListener(TimerEvent.TIMER, setAlive);
			delayTimer.start();
			
		}
		
		
		public function Update() {


			switch (carnum) {

				case 0:
					car0();
					break;
				case 1:
					car1();
					break;
				case 2:
					car2();
					break;
				case 3:
					car3();
					break;

			}
		}
	}

}