﻿package {

	import flash.display.Stage;
	import flash.display.MovieClip;


	public class CollidableComponent {


		var possessed: MovieClip;
		
		var halfHeight: Number;
		
		var halfWidth: Number;


		public function CollidableComponent(mplayer: MovieClip) {

			// constructor code
			possessed = mplayer;
			
			halfHeight = possessed.height / 2;
			halfWidth = possessed.width / 2;
			
			
		}


		public function Update() {

			CollisionTest();

		}

		public function CollisionTest() {

			//Check for collision on traffic car against player
			for (var t = 0; t < possessed.trafficArray.length; t++) {

				if (possessed.hitTestObject(possessed.trafficArray[t]) && possessed.trafficArray[t].alive) {

					possessed.iPanel.hitcar(t);
					possessed.trafficArray[t].delayHit();

				}

			}
			
//Tried another collision test, need player rotation to work			
/*			
			while(possessed.mGrass.hitTestPoint(possessed.x - halfWidth, possessed.y + halfHeight, true)) { //top left of car
				
				possessed.x++;
				possessed.mPhysicsComponent.mapfriction;
				//possessed.y++;
			}
			
			while (possessed.mGrass.hitTestPoint(possessed.x + halfWidth, possessed.y + halfHeight, true)) { //top right of car
				possessed.x--;
				possessed.mPhysicsComponent.mapfriction;
				
			}
			
			while (possessed.mGrass.hitTestPoint(possessed.x + halfWidth, possessed.y - halfHeight, true)) { //bottom left of car
				
				possessed.x++;
				possessed.mPhysicsComponent.mapfriction;
			}
			
			while (possessed.mGrass.hitTestPoint(possessed.x - halfWidth, possessed.y - halfHeight, true)) { //bottom right of car
				
				possessed.x--;
				possessed.mPhysicsComponent.mapfriction;
			}
			
*/

			
			// -- Wall Collision --
			
			if (possessed.x < 0 + possessed.radius) {

				possessed.x = 0 + possessed.radius;
				possessed.mPhysicsComponent.speed *= possessed.mPhysicsComponent.mapfriction;

			}

			if (possessed.x > possessed.pStage.stageWidth - possessed.radius) {

				possessed.x = possessed.pStage.stageWidth - possessed.radius;
				possessed.mPhysicsComponent.speed *= possessed.mPhysicsComponent.mapfriction;

			}

			if (possessed.y > possessed.pStage.stageHeight - possessed.radius) {

				possessed.y = possessed.pStage.stageHeight - possessed.radius;
				possessed.mPhysicsComponent.speed *= possessed.mPhysicsComponent.mapfriction;
			}


			if (possessed.y < 0 + possessed.radius) {

				possessed.y = 0 + possessed.radius;
				possessed.mPhysicsComponent.speed *= possessed.mPhysicsComponent.mapfriction;
			}

			
			
			//Bottom Left
			if ((possessed.x - 5 > 60 && possessed.x + 5 < 260) && (possessed.y - 5 > 460 && possessed.y + 5 < 660)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Bottom Middle Left
			if ((possessed.x - 5 > 300 && possessed.x + 5 < 580) && (possessed.y - 5 > 460 && possessed.y + 5 < 660)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Bottom Middle Right
			if ((possessed.x - 5 > 580 && possessed.x + 5 < 1220) && (possessed.y - 5 > 460 && possessed.y + 5 < 580)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Bottom Right
			if ((possessed.x - 5 > 620 && possessed.x + 5 < 1280) && (possessed.y - 5 > 620 && possessed.y + 5 < 720)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Top Left
			if ((possessed.x - 5 > 60 && possessed.x + 5 < 260) && (possessed.y - 5 > 60 && possessed.y + 5 < 420)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Top Middle Left
			if ((possessed.x - 5 > 300 && possessed.x + 5 < 500) && (possessed.y - 5 > 0 && possessed.y + 5 < 180)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Top Middle Right
			if ((possessed.x - 5 > 700 && possessed.x + 5 < 1060) && (possessed.y - 5 > 0 && possessed.y + 5 < 180)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Top Right
			if ((possessed.x - 5 > 1100 && possessed.x + 5 < 1220) && (possessed.y - 5 > 60 && possessed.y + 5 < 180)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Right Middle
			if ((possessed.x - 5 > 1020 && possessed.x + 5 < 1280) && (possessed.y - 5 > 220 && possessed.y + 5 < 420)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Middle Middle
			if ((possessed.x - 5 > 300 && possessed.x + 5 < 980) && (possessed.y - 5 > 220 && possessed.y + 5 < 420)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

			//Middle Top
			if ((possessed.x - 5 > 540 && possessed.x + 5 < 660) && (possessed.y - 5 > 60 && possessed.y + 5 < 300)) {

				possessed.mPhysicsComponent.speed = -possessed.mPhysicsComponent.speed;
			}

		}

	}

}