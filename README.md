# CakeDash 🍰

## Introduction to Game Design & Development 1st Yr Project

Final version of U-Team's game **CakeDash** for a University 1st Year Gaming Module.

### Gameplay

Drive around a 2D top-down environment to pickup cakes and increase your remaining time whilst avoiding opposing traffic and buildings, to achieve your highest score!

#### Controls

- W => Acceleration
- A => Turn Left
- S => Turn Right
- D => Slow Down, Reverse
